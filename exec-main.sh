#!/bin/bash

APP_NAME=$1

DIR_MAIN=$PWD
DIR_PARENT="$(dirname "$DIR_MAIN")"
DIR_APP=$DIR_PARENT/$APP_NAME

DATE_START=$(date +%s)

# Removing the old app (if necessary)
if [ -d "$DIR_APP" ]; then
  rm -rf $DIR_APP
fi

echo '------------------------------------------------------------------------'
echo "cd $DIR_PARENT && react-static create --name=$APP_NAME --template='basic'"
cd $DIR_PARENT && react-static create --name=$APP_NAME --template='basic'

wait

# Copy credentials.sh to the new app's root directory
echo '-------------------------------------'
echo 'Copying credentials.sh to the new app'
cp $DIR_MAIN/credentials.sh $DIR_APP

# Copy credentials.sh to the new app's root directory
echo '-------------------------------------'
echo 'Copying credentials.sh to the new app'
cp $DIR_MAIN/credentials.sh $DIR_APP

# Add the mod directory and mod scripts to .gitignore
echo '' >> $DIR_APP/.gitignore
echo '' >> $DIR_APP/.gitignore
echo '# Files and directories from React Static Neutrino' >> $DIR_APP/.gitignore
echo 'mod/*' >> $DIR_APP/.gitignore
echo 'mod-*' >> $DIR_APP/.gitignore
echo 'mod_app.sh' >> $DIR_APP/.gitignore

# Copy mod_app.sh to the new app's root directory
cp $DIR_MAIN/mod_app.sh $DIR_APP

# Copy the mod directory to the new app's root directory
cp -R $DIR_MAIN/mod $DIR_APP

# Modify the new app
cd $DIR_APP && bash mod_app.sh '01'
cd $DIR_APP && bash mod_app.sh '02'
cd $DIR_APP && bash mod_app.sh '03'
cd $DIR_APP && bash mod_app.sh '04'

# Remove the mod* files from the new app
rm -rf $DIR_APP/mod
rm $DIR_APP/mod*

echo '########################'
echo "cd $DIR_APP && sh all.sh"
cd $DIR_APP && sh all.sh

DATE_END=$(date +%s)

T_SEC=$((DATE_END-DATE_START))

echo '##########################################'
echo 'The new app has been created from scratch!'
echo ''
echo "It is located at: $DIR_APP"
echo ''
echo "Time used:"
echo "$((T_SEC/60)) minutes and $((T_SEC%60)) seconds"
echo ''
echo 'If all goes well, there are no major errors.'
echo 'Please note that there are no tests initially.'
echo ''
echo 'Further instructions on what to do next are in the'
echo 'README-to_do.txt file within your app.'
