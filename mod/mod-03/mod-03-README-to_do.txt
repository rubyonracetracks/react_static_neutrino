GETTING STARTED
1.  Start tmux.  Enter this app's root directory.  Enter the command "sh server.sh".
2.  Open your web browser to the appropriate URL to view your app locally.
3.  Start a second tmux window, and go to this app's root directory. Use this tmux window for entering commands.
4.  Start a Git repository for your new app, and push your new app into that repository.  (NOTE: As of July 2018, I prefer BitBucket.  Unfortunately, many continuous integration services do not support GitLab.)
5.  Deploy this app to Netlify.  The build command is "yarn build".  The publish directory is "dist".
6.  Update the website/package.json file.  More details are provided later in the section below.
7.  Add a continuous integration badge for the build.  (NOTE: As of July 2018, I prefer Semaphore CI, located at https://semaphoreci.com/.)
8.  Customize the README.md file and everything else in this app.  View, test, update, and deploy it frequently.

UPDATING PACKAGE.JSON
1.  In this app's root directory, go to terminal window and enter the command "yarn init".  Enter your answers to the questions asked.  This updates the package.json file.
2.  In the package.json file, add a test entry to the scripts section.  Add a comma after the last entry.  After this comma, add the line '"test": "react-static build"'.
3.  Enter the command "sh git_check.sh".  After you see that all tests pass, commit the changes. 
