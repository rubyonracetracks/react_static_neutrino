#!/bin/bash

# AGENDA:
# * Adding to do list

echo '#####################'
echo 'Chapter 3: To Do List'
echo '#####################'

mv mod-03-README-to_do.txt README-to_do.txt

git add .
git commit -m "Added to do list"
