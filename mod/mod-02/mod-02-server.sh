#!/bin/bash

echo '**********************************************************'
echo 'About to run your new React Static app on the local server'
echo 'You will be able to view your app in your browser at the following URLs:'
echo 'React Static web server: http://localhost:3000'
echo ''
echo 'NOTE: If you are using a non-zero port offset, the correct port numbers will be different.'
echo "******************************************************************************************"
cat /home/winner/shared/ports.txt

echo '----------'
echo 'yarn build'
yarn build

echo '----------'
echo "yarn start"
yarn start
