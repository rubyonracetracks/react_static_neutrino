#!/bin/bash

rm -rf node_modules
rm -rf coverage
rm -rf dist
rm -rf tmp

echo '*************'
echo 'Things to do:'
echo '1.  Stop the React-static server.'
echo '2.  Enter the command "sh all.sh; sh server.sh" to restart the server.'
