#!/bin/bash

# AGENDA:
# * Add basic scripts

echo '###############################'
echo 'Chapter 2: Adding Basic Scripts'
echo '###############################'

mv mod-02-all.sh all.sh
mv mod-02-clean.sh clean.sh
mv mod-02-git_check.sh git_check.sh
mv mod-02-server.sh server.sh
mv mod-02-test_app.sh test_app.sh
mv mod-02-test_code.sh test_code.sh

git add .
git commit -m "Added basic scripts"
