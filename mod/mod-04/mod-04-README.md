# Welcome

This app was created with the aid of React Static Neutrino, which automatically creates a new React Static app from scratch.  It's like Rails Neutrino and Docusaurus Neutrino but for React Static instead.

## Scripts
* all.sh: This script runs test_app.sh and test_code.sh.
* credentials.sh: Run this script after you reset your Docker container to enter your Git name and email address.
* test_app.sh: This script runs the commands "yarn install" and "yarn test" in the website directory.
* test_code.sh: This script runs commands that check this app for outdated  packages.
* server.sh: This script displays your app on your local server. Please note that the port number will be something other than 3000 if you used a non-zero offset.
* git_check.sh: Run this script before making a Git commit.
* clean.sh: This script removes the node_modules, coverage, dist, and tmp directories to make it easier to search for keywords in your source code.  However, you will need to stop your local server, rebuild this app, and restart your server.

## Setup Procedure
* If you are using Docker, you need at least two tmux windows.  If you have Docusaurus installed directly in your host OS, you'll need at least two terminal windows or tabs.
* In the first window, enter this app's root directory and enter the command "sh all.sh; sh server.sh".
* In the second window, enter this app's root directory.  Use this window for running the scripts and entering Git commands.
