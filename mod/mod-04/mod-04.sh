#!/bin/bash

# AGENDA:
# * Add README.md

echo '###########################'
echo 'Chapter 4: Adding README.md'
echo '###########################'

mv mod-04-README.md README.md

git add .
git commit -m "Added deployment scripts"
